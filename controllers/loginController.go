package controllers

import (
	"net/http"
	"fmt"
	"encoding/json"
	"github.com/gorilla/sessions"

	"testAuth/models"
	"golang.org/x/crypto/bcrypt"
	"encoding/hex"
	"github.com/gorilla/mux"
	"github.com/astaxie/beego/orm"
	"github.com/astaxie/beego/logs"
)
const  SESSION_NAME = "session-test_app"
const SESSION_ID = "sesion_id"
const USER_ID = "user_id"

type ResponseCode struct {
	Code        string
	Description string
	Detail      string
}

var Store *sessions.CookieStore


// validate correct session for user
func validateSession(r *http.Request ) ( *sessions.Session , error) {
	session, err := Store.Get(r, SESSION_NAME)
	session.Options.MaxAge = 0
	if err != nil {
		return nil, err
	} else {
		sessionId := session.Values[SESSION_ID]
		fmt.Println("sessionId : " ,  sessionId)
		return session,nil
	}


}

func clearSession(response http.ResponseWriter) {
	    cookie := &http.Cookie{
		         Name:   "session",
		         Value:  "",
		         Path:   "/",
		         MaxAge: -1,
		     }
	     http.SetCookie(response, cookie)
	 }
// Handler function  for  sign In , sign Up page
func RegistrationTemplate(w http.ResponseWriter, r *http.Request) {

	http.ServeFile(w, r, "views/login.html")
}
// Handler function for user profile page
// redirect to sign page if session user is not valid

func ProfileTemplate(w http.ResponseWriter, r *http.Request) {
	session, err := validateSession(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	} else {
		vars := mux.Vars(r)
		userId := vars["userid"]
		fmt.Println(" userId = ",userId)
		sessionUserId := session.Values[USER_ID]
		fmt.Println(" sessionUserId = ",sessionUserId)
		if userId != sessionUserId {
			clearSession(w)


			http.Redirect(w,r,"/",301)

		}else {
			// Save it before we write to the response/return from the handler.
			session.Save(r, w)
			http.ServeFile(w, r, "views/profile.html")
		}
	}
}
// Sign Up API function
// frontend form have contains next fields
// 	 		email - email of new user
// 			password - user's password
// ** assumption :  front end should validate this fields

func ApiSignUpUser(w http.ResponseWriter, r *http.Request) {
	session, err := validateSession(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	} else {

		email := r.FormValue("email")
		password := r.FormValue("password")

		response := ResponseCode{}
		if user, err := models.SelectUserByEmail(email);  err != orm.ErrNoRows  {
			fmt.Println("SelectUserByEmail", err.Error() )
			response.Code = "DB_ERROR"
			response.Detail = err.Error()
		} else {
					hasPass, _ := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
				user = &models.User{
					Email: email,
					Password : hex.EncodeToString(hasPass),
				}

				if err := user.AddUpdateById(); err != nil {
					fmt.Println("AddUpdateById", err.Error() )
					response.Code = "DB_ERROR"
					response.Detail = err.Error()
				}  else {
					response.Code = "OK"
					response.Detail = user.Id

					session.Values[USER_ID] = user.Id
					session.Save(r,w)
				}

		}

		json.NewEncoder(w).Encode(response)
	}
}

func ApiSignInUser(w http.ResponseWriter, r *http.Request) {
	session, err := validateSession(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	} else {
		email := r.FormValue("email")
		password := r.FormValue("password")

		response := ResponseCode{}
		if user, err := models.SelectUserByEmail(email);  err != nil  {
			fmt.Println("SelectUserByEmail", err.Error() )
			response.Code = "DB_ERROR"
			response.Detail = err.Error()
		} else {
			hashPass, err := hex.DecodeString(user.Password)
			if err != nil {
				logs.Error("crype error ", err.Error())
				response.Code = "DB_ERROR"
				response.Detail = err.Error()
			}

			err = bcrypt.CompareHashAndPassword(hashPass, []byte(password))

			if err != nil {
				logs.Error("Pass invald ")
				response.Code = "DB_ERROR"
				response.Detail = err.Error()
			} else {
				response.Code = "OK"
				session.Values[USER_ID] = user.Id
				session.Save(r,w)
			}
		}


	}
}
func ApiLogout(w http.ResponseWriter, r *http.Request) {
	session, err := validateSession(r)
	session.Values[USER_ID] = ""
	session.Save(r,w)
	http.Redirect(w,r,"/",301)
}
func ApiUpdateProfile(w http.ResponseWriter, r *http.Request) {

}

