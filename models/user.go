package models

import (
	"github.com/satori/go.uuid"
	"github.com/astaxie/beego/orm"
	"errors"
	"fmt"
)
/*
CREATE TABLE `apo`.`users` (
  `id` VARCHAR(50) NOT NULL,
  `email` VARCHAR(100) NOT NULL,
  `password` VARCHAR(100) NOT NULL,
  `fullname` VARCHAR(200) NULL,
  `address` VARCHAR(200) NULL,
  `phone` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC));

 */

var Conn orm.Ormer

var ErrExistingUser = errors.New("Existing user ")

type User struct{
	Id string `orm:"pk"`
	Email string
	Password string
	FullName string
	Address string
	Phone string
}


// function for search user by email .
// search user by email. return existing user and  error . for new User whitout database error field Id = ""
// Parameter
//  email  - email for search user
func  SelectUserByEmail(  email string ) ( *User , error ) {

	user := User{
		Email: email,
	}

	if err:= Conn.Read(&user,"Email") ; err != nil {
		fmt.Println("MD Read Email Err  ", err.Error())
		return nil , err
	} else {
		fmt.Println("MD Read Email , ", user )
		if user.Id != "" {
			return nil, ErrExistingUser
		} else {
			return &user, nil
		}
	}
}
// Insert new or update existing user
// fill struct User
// add new user if this.Id == ""
func ( this *User) AddUpdateById( ) error {

	var err error
	if this.Id == "" {
		// add new user
		// create new UUID key . then better way use autoincrement id for primary key and unique user key for identify user in web ap p
		// ( use in path parameter for instance )
		this.Id = uuid.NewV4().String()

		// check user for existing email
		checkUser := User{
			Email: this.Email,
		}

		if err = Conn.Read(&checkUser,"Email") ; err != nil  && err == orm.ErrNoRows {
			_, err = Conn.Insert(this)
			if err != nil {
				fmt.Println("Error Insert , ", err.Error())
			} else {
				fmt.Println("Ok Insert , " ,  this )
			}
		} else if err == nil {
			 err = ErrExistingUser
		}
	} else {
		// update existing user
		_ , err = Conn.Update(this)

	}
	if err == nil {
		Conn.Commit()
	} else {
		Conn.Rollback()
	}
	return nil
}


func ( this *User ) VerifyUser ( password string )  error {
	return nil
}