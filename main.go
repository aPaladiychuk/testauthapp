package main

import (
	"net/http"
	"fmt"
	"html"
	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	"log"
	"testAuth/controllers"
	"github.com/astaxie/beego/orm"
	_ "github.com/go-sql-driver/mysql"

	"testAuth/models"
)

func init() {
	// register model
	orm.RegisterModel(new(models.User))

	orm.RegisterDataBase("default", "mysql", "root:mysql@/apo?charset=utf8", 30)
}

func main() {

	models.Conn = orm.NewOrm()
	controllers.Store = sessions.NewCookieStore([]byte("test-auth-app"))


	router := mux.NewRouter().StrictSlash(true)


	router.HandleFunc("/", controllers.RegistrationTemplate )
	router.HandleFunc("/profile/{userid}", controllers.ProfileTemplate )
	// api
	router.HandleFunc("/v1/user/signup", controllers.ApiSignUpUser ).Methods("POST")
	router.HandleFunc("/v1/user/signin", controllers.ApiSignInUser ).Methods("POST")
	router.HandleFunc("/v1/user/editprofile", controllers.ApiUpdateProfile ).Methods("POST")

	router.PathPrefix("/").Handler(http.StripPrefix("/", http.FileServer(http.Dir("views/"))))

	router.HandleFunc("/v1/signap", Index)


	http.Handle("/", router)
	log.Fatal(http.ListenAndServe(":8080", router))
}

func Index(w http.ResponseWriter, r *http.Request) {
	session, err := controllers.Store.Get(r, "session-name")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	} else {
		fmt.Println(session)
	}

	fmt.Fprintf(w, "Hello, %q", html.EscapeString(r.URL.Path))

}